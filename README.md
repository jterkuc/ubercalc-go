# UberCalc-go

## What is this

SuperCalc is an amazing app designed to make sure you get the correct answer when trying to do calcuations. 

If you think back to doing a math test, you always like to go back and check to make sure you got your answer right. That may be one time, two times, or more, and just when you thought you got it right, the answer changes and you saved yourself some marks. 

The same idea is what inspired this app. Launch it, and SuperCalc will calculate basic arythmatic not once, not twice, but 100,000 times simultaniously, then will average the results and get you the most accurate answer. 
