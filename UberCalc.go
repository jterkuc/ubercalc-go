//TODO Add timer system

package main

import (
	"fmt"
)

// 1 = Addition
// 2 = Subtraction
// 3 = Multiplication
// 4 = Division
// Calculate numbers n1 and n2 and return product.

// Uses a pointer to a slice to allow for better parallelization
func Calculate(n1 int, n2 int, sign int, ThreadVal int, res map[int]int, done chan bool) {

	switch sign {
	case 1:
		res[ThreadVal] = (n1 + n2)
	case 2:
		res[ThreadVal] = (n1 - n2)
	case 3:
		res[ThreadVal] = (n1 * n2)
	case 4:
		res[ThreadVal] = (n1 / n2)
	default:
		res[ThreadVal] = 0
	}
	done <- true
}

func main() {
	ansMap := make(map[int]int)
	done := make(chan bool)

	var timesToCheck = 100000

	var num1, num2, selNum int

	fmt.Println("This is SueprCalc. \nEnter '1' for Addition\nEnter '2' for Subtraction\nEnter '3' for Multiplication\nEnter '4' for Division")
	fmt.Scanln(&selNum)

	fmt.Print("Enter your first value: ")
	fmt.Scanln(&num1)

	fmt.Print("Enter your second value: ")
	fmt.Scanln(&num2)

	for i := 0; i < timesToCheck; i++ {
		go Calculate(num1, num2, selNum, i, ansMap, done)
		<-done
	}

	var sum int

	for i := 0; i < timesToCheck; i++ {
		sum += ansMap[i]
	}

	sum = sum / timesToCheck

	fmt.Print("The answer is: ")
	fmt.Print(sum)
}
